class BaseAccount {
    constructor(accountNumber){
        console.log("Account number " + accountNumber);
    }
}
class SavingAccount extends BaseAccount{
    constructor(accountNumber){
        super(accountNumber);
    }
}

SavingAccount.prototype.getBalance = function(){
    return 100;
}

module.exports = SavingAccount;