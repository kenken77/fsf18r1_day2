// from the internet 
const chalk = require('chalk');

// custom your own lib
var SavingAccount = require('./account/saving');

console.log("Hello World !");
var x  = 1+1;
console.log(x);

var y;
console.log(y);

var yy = null;
console.log(yy);

var amount = 10;
console.log(amount);
console.log(typeof amount);
amount = true;
console.log(amount);
console.log(typeof amount);


var x = 5;
const x2 = 10;
function A(){
    let x = true;
    let x2 = 100;
    console.log("Inside A " + x);
    console.log(x2);
}

A();
console.log(x);
console.log(x2);

var firstName = "Kenneth";
var lastName = 'Kenneth';
console.log(typeof lastName);

var person = {
    'first name' : 'Ken',
    age : 40,
    gender : 'Male',
    fruits: ['Apple', 'Orange']
}

console.log(person['first name']);
console.log(person.fruits[1]);

var fruits = ['durian', 1, true]

console.log(fruits);

console.log(fruits[1]);

console.log(fruits[4]);

console.log(fruits.length);

console.log(fruits.indexOf(1));
var yy = 'Kenneth';
switch (yy){
    case 'Kenneth':
        console.log("its Ken");
        break;
    case 10:
        console.log("its Aileen");
        break;
    default:
        console.log('everything !');
}

var x = true;
if(x){
    console.log('its x');
}else{
    console.log('not x');
}

function hi1(){
    console.log('hi 1');
}

function hi2(){
    console.log('hi 2');
}

function hiAll(cb1, cb2){
    cb1();
    cb2();
}

//hiAll(hi1, hi2);

setTimeout(function(){
    //hiAll(hi1, hi2);
}, 5000);

console.log("----------------");

var fruits2 = ['durian', 'Apple', 'orange', 'watermelon'];

for(var x = 0; x < fruits2.length; x++){
    console.log(fruits2[x]);
    if(fruits2[x] == 'orange'){
        console.log('EXIT');
        break; // line 106
    }
}


fruits2.forEach(function(value, index){
    console.log(value);
    console.log(index); 
});


fruits2.forEach((value, index)=>{
    console.log(value);
    console.log(index);    
});

fruits2.push('dragonfruit');

console.log(fruits2);

fruits2.pop();

console.log(fruits2);

var result = fruits2.slice(1,3);
console.log('--> ' + fruits2);
console.log(result);

fruits2.splice(1,3, 'jackfruit');
console.log(fruits2);

fruits2.push('durian');
fruits2.push('mangosteen');
console.log(fruits2);

fruits2.shift();
console.log(fruits2);

fruits2.unshift('Banana');
console.log(fruits2);

var numbers = [ 3,4,5,2,7,8 ];

// numbers.sort( (a, b)=>{
//     return a - b;
// });
fruits2.push('banana');
console.log("Before sorting ... " + fruits2);
numbers.sort();
fruits2.sort();
console.log("after sorting ... " + fruits2);

console.log(numbers);
console.log('> ' + numbers.reverse());

const  AA = function AAA(){
    console.log('AAA');
    return 1;
}

function AAAA(){

}
AA();
var returnAA = AA();
console.log(returnAA);
console.log("AA = " + AA);
console.log("AAA = " + AAAA);

var person2 = {
    first_name: 'Kenneth',
    last_name: 'Phang',
    age: 40,
    hello: function(){
        let ic_no = 'FSDFDSF';
        return 'hello kitty';
    }
}

console.log(person2.first_name);
console.log(person2.hello());

var publisher = {
    name: "ABC Pte Ltd",
    address: 'Singapore'
}

var publishDates = [
                {revision: 1, date:'19 Nov 2017'}, 
                {revision: 2, date:'19 Nov 2018'}, 
                {revision: 3, date:'19 Nov 2019'}
            ]

/*

*/
var harryPotterBook = {
    title: 'Dragon La la ',
    publisher: publisher,
    publishDates: publishDates,
    inStock: 100
}

console.log(harryPotterBook.publisher.name);
console.log(harryPotterBook);
delete harryPotterBook.inStock;
console.log(harryPotterBook);
harryPotterBook.inStock = 50;
console.log(harryPotterBook);
harryPotterBook.sales = 50000;
console.log(harryPotterBook);

function Person4(_age){
    var first_name = "Kenneth";
    this.last_name= "Phang"
    this.age = _age;
    this.fullName =  first_name +  ' ' + this.last_name;
}


const personX = new Person4();
console.log(personX.last_name);
console.log(personX.first_name);
console.log(personX.age);
console.log(personX.fullName);

var Person5 =  function(_name){
    this.name = _name;
}

Person5.prototype.sayHello = function() {
    console.log('new test function for personX');
};

const newPerson5  = new Person5('Kenneth');
var newPerson6  = new Person5('Alice');
var newPerson7  = new Person5('Emily');

const n2 = 10;

console.log(newPerson5);
newPerson5.sayHello();
newPerson5.age = 25;
console.log(newPerson5.age);
// const object cannot be replace 
// 5 equals 6 cannot !
newPerson6 = newPerson5;
// newPerson5 = newPerson6; <-- error !
console.log(newPerson5);
delete newPerson5.age;
console.log(newPerson5);
//console.log(Person5.sayHello);

if(newPerson5 === newPerson6){
    console.log("Same !");
}else{
    console.log("Not Same !");
}

console.log(newPerson5.name);
console.log(newPerson6.name);

if(newPerson5.name == newPerson7.name){
    console.log("Same !");
}else{
    console.log("Not Same !");
}

// variable fallback
function defaultParams(paramValue){
    console.log(paramValue);
    let value2 = paramValue || 4000;
    console.log(value2);
}

defaultParams(5000);

// var yy = 5;
// yy++;
// console.log(yy);
// console.log(typeof yy);

// string template / interpolation
var first_name = 'Kenneth';
var last_name = 'Phang';

var fullName = 'My fullname is ' + first_name + ' ' + last_name;
console.log(fullName);
var fullName = `My NEW fullname is ${first_name} ${last_name}`;
console.log(fullName);

// Promise

setTimeout(function(){
    console.log("Dispense Money 1");
}, 2000);

setTimeout(function(){
    console.log("Dispense Money 2 ");
}, 1500);

var orderNo = 12345;

var waitFor = new Promise((resolve, reject)=>{
    setTimeout(resolve, 2000);
}).then(()=>{
    if(orderNo == 12345){
        console.log("Here is your BIG MAC ! ");
    }else{
        throw new Error('Error !');
    }
}).catch((error)=>{
    console.log(error);
    reject(orderNo);
})

console.log("xxx");

class Fruit {
    constructor(_name2){
        this.name = _name2;
    }

    getQuantity(){
        return this.quantity;
    }

    setQuantity(_quantity){
        this.quantity = _quantity;
    }
}

class Orange extends Fruit{
    constructor(_name){
        super(_name);
    }

    getQuantity_2(){
        return 20;
    }
}

let s  = new Orange('mandarin');
s.setQuantity(100)
console.log("Orange > " + s.getQuantity());
console.log("Orange > " + s.getQuantity_2());

var acc = new SavingAccount(12345);
console.log(chalk.blue(acc.getBalance()));
const log = console.log;
log(chalk.blue('Hello') + ' World' + chalk.red('!'));
 